package com.night.forum.services;

import com.night.forum.dto.UserDto;
import com.night.forum.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

    @Autowired
    private RestTemplate restTemplate;

    private static final String FIND_USER_BY_ID = "http://auth.nuitinfo.oprax.fr/users/%s";

    /**
     * Find a user by id
     * @param id user id
     * @return user found
     */
    public UserDto findUserById(Long id) {
        ResponseEntity<UserDto> response = restTemplate.getForEntity(
            String.format(FIND_USER_BY_ID, id),
                UserDto.class
        );

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        throw new UserNotFoundException("The user doesn't exist");
    }
}
