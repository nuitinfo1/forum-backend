package com.night.forum.services;

import com.night.forum.dto.CategoryDto;
import com.night.forum.entities.CategoryEntity;
import com.night.forum.exceptions.CategoryNotFoundException;
import com.night.forum.repositories.ICategoryRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService extends BaseService<CategoryDto> {

    CategoryService(@Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Autowired
    private ICategoryRepository categoryRepository;

    /**
     * Persist a category in database
     * @param categoryDto to persist
     * @return persisted category
     */
    @Override
    public CategoryDto add(CategoryDto categoryDto) {
        return modelMapper.map(
                categoryRepository.save(
                        modelMapper.map(categoryDto, CategoryEntity.class)
                ),
                CategoryDto.class
        );
    }

    /**
     * Find all categories
     * @return all categories
     */
    @Override
    public List<CategoryDto> findAll() {
        return modelMapper.map(
                categoryRepository.findAll(),
                new TypeToken<List<CategoryDto>>(){}.getType()
        );
    }

    /**
     * Find a category by id
     * @param id to find the category
     * @return the category found
     */
    @Override
    public CategoryDto findById(Long id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);

        if (categoryEntity.isEmpty()) {
           throw new CategoryNotFoundException("The category doesn't exist");
        }

        return modelMapper.map(
                categoryEntity.get(),
                CategoryDto.class
        );
    }

    /**
     * Delete a category
     * @param id of the category to delete
     */
    @Override
    public void delete(Long id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);

        if (categoryEntity.isEmpty()) {
            throw new CategoryNotFoundException("The category doesn't exist");
        }

        categoryRepository.delete(
                modelMapper.map(
                        categoryEntity.get(),
                        CategoryEntity.class
                )
        );
    }

    /**
     * Update a category
     * @param categoryDto to update
     * @return the updated category
     */
    @Override
    public CategoryDto update(CategoryDto categoryDto) {
        return modelMapper.map(
                categoryRepository.save(
                        modelMapper.map(categoryDto, CategoryEntity.class)
                ),
                CategoryDto.class
        );
    }
}
