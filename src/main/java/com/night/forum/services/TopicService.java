package com.night.forum.services;

import com.night.forum.dto.TopicDto;
import com.night.forum.entities.TopicEntity;
import com.night.forum.exceptions.TopicNotFoundException;
import com.night.forum.repositories.ITopicRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TopicService extends BaseService<TopicDto> {

    @Autowired
    private ITopicRepository topicRepository;

    TopicService(@Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    /**
     * Persist a topic
     * @param topicDto to persist
     * @return the persisted topic
     */
    @Override
    public TopicDto add(TopicDto topicDto) {
        return modelMapper.map(
                topicRepository.save(
                        modelMapper.map(topicDto, TopicEntity.class)
                ),
                TopicDto.class
        );
    }

    /**
     * Find all topics
     * @return all topics
     */
    @Override
    public List<TopicDto> findAll() {
        return modelMapper.map(
                topicRepository.findAll(),
                new TypeToken<List<TopicDto>>(){}.getType()
        );
    }

    /**
     * Find a topic by id
     * @param id to find the topic
     * @return the topic found
     */
    @Override
    public TopicDto findById(Long id) {
        Optional<TopicEntity> topic = topicRepository.findById(id);

        if (topic.isEmpty()) {
            throw new TopicNotFoundException("The topic doesn't exist");
        }

        return modelMapper.map(
                topic.get(),
                TopicDto.class
        );
    }

    /**
     * Delete a topic
     * @param id of the topic to delete
     */
    @Override
    public void delete(Long id) {
        Optional<TopicEntity> topic = topicRepository.findById(id);

        if (topic.isEmpty()) {
            throw new TopicNotFoundException("The topic doesn't exist");
        }

        topicRepository.delete(
                modelMapper.map(
                        topic.get(),
                        TopicEntity.class
                )
        );
    }

    /**
     * Update a topic
     * @param topicDto to update
     * @return the updated topic
     */
    @Override
    public TopicDto update(TopicDto topicDto) {
        return modelMapper.map(
                topicRepository.save(
                        modelMapper.map(topicDto, TopicEntity.class)
                ),
                TopicDto.class
        );
    }

    /**
     * Find all topics by category id
     * @param id category id
     * @return all topics by category id
     */
    public List<TopicDto> findAllByCategoryId(Long id) {
        return modelMapper.map(
                topicRepository.findAllByCategory_IdOrderByDateTimeDesc(id),
                new TypeToken<List<TopicDto>>(){}.getType()
        );
    }
}
