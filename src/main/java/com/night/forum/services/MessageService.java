package com.night.forum.services;

import com.night.forum.dto.MessageDto;
import com.night.forum.entities.MessageEntity;
import com.night.forum.exceptions.MessageNotFoundException;
import com.night.forum.repositories.IMessageRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MessageService extends BaseService<MessageDto> {

    @Autowired
    private IMessageRepository messageRepository;

    @Autowired
    private UserService userService;

    MessageService(@Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    /**
     * Persist a message
     * @param messageDto to persist
     * @return the persisted message
     */
    @Override
    public MessageDto add(MessageDto messageDto) {
        messageDto.setDateTime(LocalDateTime.now());

        return modelMapper.map(
                messageRepository.save(
                        modelMapper.map(messageDto, MessageEntity.class)
                ),
                MessageDto.class
        );
    }

    /**
     * Find all messages
     * @return all messages
     */
    @Override
    public List<MessageDto> findAll() {
        return messageRepository.findAll()
                .parallelStream()
                .map(messageEntity -> {
                    MessageDto messageDto = modelMapper.map(messageEntity, MessageDto.class);
                    messageDto.setUser(
                            userService.findUserById(messageEntity.getUserId())
                    );

                    return messageDto;
                })
                .collect(Collectors.toList());
    }

    /**
     * Find a message by id
     * @param id to find the message
     * @return the message found
     */
    @Override
    public MessageDto findById(Long id) {
        Optional<MessageEntity> message = messageRepository.findById(id);

        if (message.isEmpty()) {
            throw new MessageNotFoundException("The message doesn't exist");
        }

        MessageDto messageDto = modelMapper.map(
                message.get(),
                MessageDto.class
        );

        messageDto.setUser(
            userService.findUserById(message.get().getUserId())
        );

        return messageDto;
    }

    /**
     * Delete a message
     * @param id of the message to delete
     */
    @Override
    public void delete(Long id) {
        Optional<MessageEntity> message = messageRepository.findById(id);

        if (message.isEmpty()) {
            throw new MessageNotFoundException("The message doesn't exist");
        }

        messageRepository.delete(
                modelMapper.map(
                        message.get(),
                        MessageEntity.class
                )
        );
    }

    /**
     * Update a message
     * @param messageDto to update
     * @return the updated message
     */
    @Override
    public MessageDto update(MessageDto messageDto) {
        return modelMapper.map(
                messageRepository.save(
                        modelMapper.map(messageDto, MessageEntity.class)
                ),
                MessageDto.class
        );
    }

    /**
     * Find all messages by topic id
     * @param id topic id
     * @return all messages by topic id
     */
    public List<MessageDto> findAllByTopicId(Long id) {
        return modelMapper.map(
                messageRepository.findAllByTopic_IdOrderByDateTimeDesc(id),
                new TypeToken<List<MessageDto>>(){}.getType()
        );
    }
}
