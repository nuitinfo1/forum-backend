package com.night.forum.services;

import org.modelmapper.ModelMapper;

import java.util.List;

public abstract class BaseService<T> {
    protected ModelMapper modelMapper;

    /**
     * Generic method to create an object
     * @param dto to create
     * @return created object
     */
    public abstract T add(T dto);

    /**
     * Generic method to find all objects
     * @return all objects
     */
    public abstract List<T> findAll();

    /**
     * Generic method to find one object by id
     * @param id to find the object
     * @return the object found
     */
    public abstract T findById(Long id);

    /**
     * Generic method to delete an object
     * @param id of the object to delete
     */
    public abstract void delete(Long id);

    /**
     * Generic method to update an object
     * @param dto to update
     * @return updated object
     */
    public abstract T update(T dto);
}
