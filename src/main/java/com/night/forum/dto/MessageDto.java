package com.night.forum.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto {
    private Long messageId;
    private String message;
    private LocalDateTime dateTime;
    private TopicDto topic;
    private UserDto user;
}
