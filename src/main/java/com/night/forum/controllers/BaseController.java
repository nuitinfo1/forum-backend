package com.night.forum.controllers;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface BaseController<T> {
    /**
     * Generic method to create an object
     * @param dto to create
     * @return created object
     */
    ResponseEntity<T> add(T dto);

    /**
     * Generic method to find all objects
     * @return all objects
     */
    ResponseEntity<List<T>> findAll();

    /**
     * Generic method to find one object by id
     * @param id to find the object
     * @return the object found
     */
    ResponseEntity<T> findById(Long id);

    /**
     * Generic method to delete an object
     * @param id of the object to delete
     */
    void delete(Long id);

    /**
     * Generic method to update an object
     * @param dto to update
     * @return updated object
     */
    ResponseEntity<T> update(T dto);
}
