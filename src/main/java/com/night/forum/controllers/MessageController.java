package com.night.forum.controllers;

import com.night.forum.dto.MessageDto;
import com.night.forum.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/messages")
@CrossOrigin(origins = "*")
public class MessageController implements BaseController<MessageDto> {

    @Autowired
    private MessageService messageService;

    /**
     * Persist a message
     * @param messageDto to persist
     * @return the persisted message
     */
    @Override
    @PostMapping("/add")
    public ResponseEntity<MessageDto> add(@RequestBody MessageDto messageDto) {
        return ResponseEntity.status(HttpStatus.OK).body(messageService.add(messageDto));
    }

    /**
     * Find all messages
     * @return all messages
     */
    @Override
    @GetMapping("/get")
    public ResponseEntity<List<MessageDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * Find a message by id
     * @param id to find the message
     * @return the message found
     */
    @Override
    @GetMapping("/get/{id}")
    public ResponseEntity<MessageDto> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(messageService.findById(id));
    }

    /**
     * Delete a message
     * @param id of the message to delete
     */
    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        messageService.delete(id);
    }

    /**
     * Update a message
     * @param messageDto to update
     * @return the updated message
     */
    @Override
    @PostMapping("/update")
    public ResponseEntity<MessageDto> update(@RequestBody MessageDto messageDto) {
        return ResponseEntity.status(HttpStatus.OK).body(messageService.add(messageDto));
    }

    /**
     * Find all messages by topic id
     * @param id topic id
     * @return all messages by topic id
     */
    @GetMapping("/topics/{id}")
    public ResponseEntity<List<MessageDto>> findAllByTopicId(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(messageService.findAllByTopicId(id));
    }
}
