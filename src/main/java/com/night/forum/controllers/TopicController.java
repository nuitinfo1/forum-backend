package com.night.forum.controllers;

import com.night.forum.dto.TopicDto;
import com.night.forum.services.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/topics")
public class TopicController implements BaseController<TopicDto> {

    @Autowired
    private TopicService topicService;

    /**
     * Persist a topic
     * @param topicDto to persist
     * @return the persisted topic
     */
    @Override
    @PostMapping("/add")
    public ResponseEntity<TopicDto> add(@RequestBody TopicDto topicDto) {
        return ResponseEntity.status(HttpStatus.OK).body(topicService.add(topicDto));
    }

    /**
     * Find all topics
     * @return all topics
     */
    @Override
    @GetMapping("/get")
    public ResponseEntity<List<TopicDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(topicService.findAll());
    }

    /**
     * Find a topic by id
     * @param id to find the topic
     * @return the topic found
     */
    @Override
    @GetMapping("/get/{id}")
    public ResponseEntity<TopicDto> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(topicService.findById(id));
    }

    /**
     * Delete a topic
     * @param id of the topic to delete
     */
    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        topicService.delete(id);
    }

    /**
     * Update a topic
     * @param topicDto to update
     * @return the updated topic
     */
    @Override
    @PostMapping("/update")
    public ResponseEntity<TopicDto> update(@RequestBody TopicDto topicDto) {
        return ResponseEntity.status(HttpStatus.OK).body(topicService.add(topicDto));
    }

    /**
     * Find all topics by category id
     * @param id category id
     * @return all topics by category id
     */
    @GetMapping("/categories/{id}")
    public ResponseEntity<List<TopicDto>> findAllByCategoryId(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(topicService.findAllByCategoryId(id));
    }
}
