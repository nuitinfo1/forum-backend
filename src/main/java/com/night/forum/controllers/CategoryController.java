package com.night.forum.controllers;

import com.night.forum.dto.CategoryDto;
import com.night.forum.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/categories")
@CrossOrigin(origins = "*")
public class CategoryController implements BaseController<CategoryDto> {

    @Autowired
    private CategoryService categoryService;

    /**
     * Persist a category in database
     * @param categoryDto to persist
     * @return persisted category
     */
    @Override
    @PostMapping("/add")
    public ResponseEntity<CategoryDto> add(@RequestBody CategoryDto categoryDto) {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.add(categoryDto));
    }

    /**
     * Find all categories
     * @return all categories
     */
    @Override
    @GetMapping("/get")
    public ResponseEntity<List<CategoryDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.findAll());
    }

    /**
     * Find a category by id
     * @param id to find the category
     * @return the category found
     */
    @Override
    @GetMapping("/get/{id}")
    public ResponseEntity<CategoryDto> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.findById(id));
    }

    /**
     * Delete a category
     * @param id of the category to delete
     */
    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        categoryService.delete(id);
    }

    /**
     * Update a category
     * @param categoryDto to update
     * @return the updated category
     */
    @Override
    @PostMapping("/update")
    public ResponseEntity<CategoryDto> update(@RequestBody CategoryDto categoryDto) {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.update(categoryDto));
    }
}
