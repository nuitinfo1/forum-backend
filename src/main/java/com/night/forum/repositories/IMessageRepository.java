package com.night.forum.repositories;

import com.night.forum.entities.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMessageRepository extends JpaRepository<MessageEntity, Long> {
    List<MessageEntity> findAllByTopic_IdOrderByDateTimeDesc(Long id);
}
