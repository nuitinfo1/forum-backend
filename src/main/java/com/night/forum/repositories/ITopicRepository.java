package com.night.forum.repositories;

import com.night.forum.entities.TopicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITopicRepository extends JpaRepository<TopicEntity, Long> {
    List<TopicEntity> findAllByCategory_IdOrderByDateTimeDesc(Long id);
}
