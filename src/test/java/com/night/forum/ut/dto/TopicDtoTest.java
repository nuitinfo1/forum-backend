package com.night.forum.ut.dto;

import com.night.forum.dto.TopicDto;
import com.night.forum.ut.ApplicationUnitTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;

class TopicDtoTest extends ApplicationUnitTest {
    private TopicDto topicDto = new TopicDto(
            1L,
            "topicName",
            LocalDateTime.of(2019, Month.DECEMBER, 6, 0, 11, 1),
            null
    );

    @Test
    void testBuilder() {
        TopicDto.TopicDtoBuilder result = TopicDto.builder();
        Assertions.assertNotNull(result);
    }

    @Test
    void testSetTopicId() {
        topicDto.setTopicId(1L);
    }

    @Test
    void testSetTopicName() {
        topicDto.setTopicName("topicName");
    }

    @Test
    void testSetDateTime() {
        topicDto.setDateTime(
                LocalDateTime.of(2019, Month.DECEMBER, 6, 0, 11, 1)
        );
    }

    @Test
    void testHashCode() {
        int result = topicDto.hashCode();
        Assertions.assertEquals(-950586546, result);
    }

    @Test
    void testToString() {
        String result = topicDto.toString();
        Assertions.assertEquals(
                "TopicDto(topicId=1, topicName=topicName, dateTime=2019-12-06T00:11:01, category=null)",
                result
        );
    }
}
