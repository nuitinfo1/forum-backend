package com.night.forum.ut.dto;

import com.night.forum.dto.MessageDto;
import com.night.forum.ut.ApplicationUnitTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;

class MessageDtoTest extends ApplicationUnitTest {
    private MessageDto messageDto = new MessageDto(
            1L,
            "message",
            LocalDateTime.of(2019, Month.DECEMBER, 6, 0, 10, 40),
            null,
            null
    );

    @Test
    void testBuilder() {
        MessageDto.MessageDtoBuilder result = MessageDto.builder();
        Assertions.assertNotNull(result.toString());
    }

    @Test
    void testSetMessageId() {
        messageDto.setMessageId(1L);
    }

    @Test
    void testSetMessage() {
        messageDto.setMessage("message");
    }

    @Test
    void testSetDateTime() {
        messageDto.setDateTime(
                LocalDateTime.of(2019, Month.DECEMBER, 6, 0, 10, 40)
        );
    }
    @Test
    void testHashCode() {
        int result = messageDto.hashCode();
        Assertions.assertEquals(-21384640, result);
    }

    @Test
    void testToString() {
        String result = messageDto.toString();
        Assertions.assertEquals(
                "MessageDto(messageId=1, message=message, dateTime=2019-12-06T00:10:40, topic=null, user=null)",
                result
        );
    }
}
