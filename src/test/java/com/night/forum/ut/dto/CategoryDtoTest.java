package com.night.forum.ut.dto;

import com.night.forum.dto.CategoryDto;
import com.night.forum.ut.ApplicationUnitTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;


class CategoryDtoTest extends ApplicationUnitTest{
    private CategoryDto categoryDto = new CategoryDto(
            1L,
            "categoryName",
            LocalDateTime.of(2019, Month.DECEMBER, 6, 0, 6, 13)
    );

    @Test
    void testBuilder() {
        CategoryDto.CategoryDtoBuilder result = CategoryDto.builder();
        Assertions.assertEquals(
                "CategoryDto.CategoryDtoBuilder(categoryId=null, categoryName=null, dateTime=null)",
                result.toString()
        );
    }

    @Test
    void testSetCategoryId() {
        categoryDto.setCategoryId(1L);
    }

    @Test
    void testSetCategoryName() {
        categoryDto.setCategoryName("categoryName");
    }

    @Test
    void testSetDateTime() {
        categoryDto.setDateTime(
                LocalDateTime.of(2019, Month.DECEMBER, 6, 0, 6, 13)
        );
    }

    @Test
    void testHashCode() {
        int result = categoryDto.hashCode();
        Assertions.assertEquals(-1291799521, result);
    }

    @Test
    void testToString() {
        String result = categoryDto.toString();
        Assertions.assertEquals("CategoryDto(categoryId=1, categoryName=categoryName, dateTime=2019-12-06T00:06:13)", result);
    }
}
