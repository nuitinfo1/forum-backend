package com.night.forum.it.services;

import com.night.forum.dto.CategoryDto;
import com.night.forum.exceptions.CategoryNotFoundException;
import com.night.forum.it.ApplicationIntegrationTests;
import com.night.forum.services.CategoryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CategoryServiceITest extends ApplicationIntegrationTests {

    @Autowired
    private CategoryService categoryService;

    private static Long categoryCreatedId;

    @Test
    @Order(1)
    public void addTest() {
        CategoryDto category = CategoryDto
                .builder()
                .categoryName("Test")
                .dateTime(LocalDateTime.now())
                .build();

        category = categoryService.add(category);
        Assert.assertNotNull(category);
        Assert.assertNotNull(category.getCategoryId());

        categoryCreatedId = category.getCategoryId();
    }

    @Test
    @Order(2)
    public void findAllTest() {
        List<CategoryDto> categories = categoryService.findAll();
        Assert.assertNotNull(categories);
        Assert.assertFalse(categories.isEmpty());
    }

    @Test
    @Order(3)
    public void findByIdTest() {
        CategoryDto category = categoryService.findById(categoryCreatedId);
        Assert.assertNotNull(category);
        Assert.assertEquals(categoryCreatedId, category.getCategoryId());
    }

    @Test
    @Order(4)
    public void updateTest() {
        CategoryDto category = categoryService.findById(categoryCreatedId);
        category.setCategoryName("New name");
        category = categoryService.update(category);

        Assert.assertNotNull(category);
        Assert.assertNotNull(category.getCategoryId());
        Assert.assertEquals("New name", category.getCategoryName());
    }

    @Test(expected = CategoryNotFoundException.class)
    @Order(5)
    public void deleteTest() {
        categoryService.delete(categoryCreatedId);
        categoryService.findById(categoryCreatedId);
    }
}
