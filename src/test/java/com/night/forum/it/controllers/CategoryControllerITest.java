package com.night.forum.it.controllers;

import com.night.forum.dto.CategoryDto;
import com.night.forum.it.ApplicationIntegrationTests;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Ignore
public class CategoryControllerITest extends ApplicationIntegrationTests {

    private static RestTemplate restTemplate;
    private static Long categoryCreatedId;

    private static final String ADD_URL = "http://localhost:8080/api/categories/add";
    private static final String UPDATE_URL = "http://localhost:8080/api/categories/update";
    private static final String FIND_ALL_URL = "http://localhost:8080/api/categories/get";
    private static final String FIND_BY_ID_URL = "http://localhost:8080/api/categories/get/%s";
    private static final String DELETE_URL = "http://localhost:8080/api/categories/delete/%s";

    @BeforeClass
    public static void setUp() {
        restTemplate = new RestTemplate();
    }

    @Test
    @Order(1)
    public void addTest() {
        CategoryDto category = CategoryDto
                .builder()
                .categoryName("Test")
                .dateTime(LocalDateTime.now())
                .build();

        ResponseEntity<CategoryDto> categoryResponseEntity = restTemplate.postForEntity(
                ADD_URL,
                category,
                CategoryDto.class
        );

        category = categoryResponseEntity.getBody();
        Assert.assertNotNull(categoryResponseEntity);
        Assert.assertNotNull(category);
        Assert.assertNotNull(category.getCategoryId());

        categoryCreatedId = category.getCategoryId();
    }

    @Test
    @Order(2)
    public void findAllTest() {
        ResponseEntity<CategoryDto[]> categoriesResponseEntity = restTemplate.getForEntity(
                FIND_ALL_URL,
                CategoryDto[].class
        );
        List<CategoryDto> categories = Arrays.asList(Objects.requireNonNull(categoriesResponseEntity.getBody()));
        Assert.assertNotNull(categoriesResponseEntity);
        Assert.assertNotNull(categories);
        Assert.assertFalse(categories.isEmpty());
    }

    @Test
    @Order(3)
    public void findByIdTest() {
        ResponseEntity<CategoryDto> categoryResponseEntity = restTemplate.getForEntity(
                String.format(FIND_BY_ID_URL, categoryCreatedId),
                CategoryDto.class
        );
        CategoryDto category = categoryResponseEntity.getBody();
        Assert.assertNotNull(categoryResponseEntity);
        Assert.assertNotNull(category);
        Assert.assertEquals(categoryCreatedId, category.getCategoryId());
    }

    @Test
    @Order(4)
    public void updateTest() {
        CategoryDto category = CategoryDto
                .builder()
                .categoryId(categoryCreatedId)
                .categoryName("New name")
                .dateTime(LocalDateTime.now())
                .build();

        ResponseEntity<CategoryDto> categoryResponseEntity = restTemplate.postForEntity(
                UPDATE_URL,
                category,
                CategoryDto.class
        );

        category = categoryResponseEntity.getBody();
        Assert.assertNotNull(categoryResponseEntity);
        Assert.assertNotNull(category);
        Assert.assertNotNull(category.getCategoryId());
        Assert.assertEquals("New name", category.getCategoryName());
    }

    @Test(expected = HttpClientErrorException.class)
    @Order(5)
    public void deleteTest() {
        restTemplate.delete(
                String.format(DELETE_URL, categoryCreatedId)
        );

        CategoryDto categoryDto = restTemplate.getForEntity(
                String.format(FIND_BY_ID_URL, categoryCreatedId),
                CategoryDto.class
        )
                .getBody();

        Assert.assertNull(categoryDto);
    }
}
