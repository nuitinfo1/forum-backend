package com.night.forum.it;

import com.night.forum.ForumApplication;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = ForumApplication.class)
@PropertySource("classpath:application-test.properties")
@Ignore
public class ApplicationIntegrationTests {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
